# Todeslang Parser

See also:

* https://gitlab.com/hakabahitoyo/todeslang-spec
* https://gitlab.com/hakabahitoyo/todeslang-interpreter

## Install

```
sudo apt install build-essential
cd src
make clean
make
sudo make install
```

## Usage

```
thel < input.txt > output.cjson
```

