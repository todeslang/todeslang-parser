#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>
#include <cassert>
#include <sstream>

#include "thel.h"


using namespace thel;
using namespace std;


static bool is_white_space_line (string in)
{
	for (auto c: in) {
		if (! (c == ' ' || c == '\t')) {
			return false;
		}
	}
	return true;
}


static string parse_directive (string in, unsigned int line_number)
{
	unsigned int index = 0;
	unsigned int eaten = 0;
	bool ok = false;
	auto node_directive = parseDirective (in, index, eaten, ok);

	if (ok && eaten == in.size ()) {
		return node_directive->format ();
	}
	return string {};
}


int main (int argc, char *argv [])
{
	bool check = false;

	if (1 < argc) {
		if (string {argv [1]} == string {"--test"}) {
			test ();
			exit (0);
		} else if (string {argv [1]} == string {"--check"}) {
			check = true;
		} else {
			cout << "thel < input.txt > output.cjson" << endl;
			exit (0);
		}
	}

	unsigned int state = 0;
	string directive_code;
	unsigned int directive_line_number = 1;

	for (unsigned int line_number = 1; ; line_number ++) {
		if (cin.eof ()) {
			break;
		}

		string buffer;
		getline (cin, buffer);

		switch (state) {
		case 0:
			if (is_white_space_line (buffer)) {
				if (! directive_code.empty ()) {
					string out = parse_directive (directive_code, directive_line_number);
					if (check) {
						if (out.empty ()) {
							cout
								<< "Syntax error after the line "
								<< directive_line_number << "."
								<< endl;
						}
					} else {
						if (out.empty ()) {
							cerr
								<< "Syntax error after the line "
								<< directive_line_number << "."
								<< endl;
						} else {
							cout << out << endl;
						}
					}
				}
				directive_code.clear ();
				state = 1;
			} else {
				directive_code += buffer;
			}
			break;
		case 1:
			if (is_white_space_line (buffer)) {
				/* Do nothing. */
			} else {
				directive_code += buffer;
				directive_line_number = line_number;
				state = 0;
			}
			break;
		default:
			assert (false);
			break;
		}
	}

	if (! directive_code.empty ()) {
		string out = parse_directive (directive_code, directive_line_number);
		if (check) {
			if (out.empty ()) {
				cout
					<< "Syntax error after the line "
					<< directive_line_number << "."
					<< endl;
			}
		} else {
			if (out.empty ()) {
				cerr
					<< "Syntax error after the line "
					<< directive_line_number << "."
					<< endl;
			} else {
				cout << out << endl;
			}
		}
	}

	return 0;
}


