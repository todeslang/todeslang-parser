#include <iostream>

#include "thel.h"


using namespace std;
using namespace thel;


void thel::test ()
{
	{
		string code {"trans"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		string directive_name = parseDirectiveName (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << endl;
	}

	{
		string code {"continuation"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		string directive_name = parseDirectiveName (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << endl;
	}

	{
		string code {"A"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		parseSpace (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << endl;
	}

	{
		string code {"\t\tA"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		parseSpace (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << endl;
	}

	{
		string code {"this is a pen"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		string literal_string = parseString (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << " [" << literal_string << "]" <<endl;
	}

	{
		string code {"this is a pen   "};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		string literal_string = parseString (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << " [" << literal_string << "]" <<endl;
	}

	{
		string code {"{WHAT! THE! FUCK!!!}"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		string literal_string = parseString (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << " [" << literal_string << "]" <<endl;
	}

	{
		string code {"^@"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		string literal_string = parseString (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << " [" << literal_string << "]" <<endl;
	}

	{
		string code {"^$"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		string literal_string = parseString (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << " [" << literal_string << "]" <<endl;
	}

	cout << endl;

	{
		string code {"$main"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseParameterExpression (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << endl;
		cout << expression->format () << endl << endl;
	}

	{
		string code {"$$action"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseParameterExpression (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << endl;
		cout << expression->format () << endl << endl;
	}

	{
		string code {"%"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseSpecialExpression (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << endl;
		cout << expression->format () << endl << endl;
	}

	{
		string code {"^true"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseSpecialExpression (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << endl;
		cout << expression->format () << endl << endl;
	}

	{
		string code {"^false"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseSpecialExpression (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << endl;
		cout << expression->format () << endl << endl;
	}

	{
		string code {"#ffff"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseUnaryOperatorExpression (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << endl;
		cout << expression->format () << endl << endl;
	}

	{
		string code {"$a"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseNonOperatorExpression (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << endl;
		cout << expression->format () << endl << endl;
	}

	{
		string code {"$a + $b ^as string"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseLeftJoinOperatorExpression (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << endl;
		cout << expression->format () << endl << endl;
	}

	{
		string code {"$a + ($b ^as number)"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseLeftJoinOperatorExpression (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << endl;
		cout << expression->format () << endl << endl;
	}

	{
		string code {"#1 + #2 < #3"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseNoJoinOperatorExpression (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << endl;
		cout << expression->format () << endl << endl;
	}

	{
		string code {"$b"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseExpression (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << endl;
		cout << expression->format () << endl << endl;
	}

	{
		string code {"#1 + #2 + #3"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseExpression (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << endl;
		cout << expression->format () << endl << endl;
	}

	{
		string code {"[action: main;]"};
		unsigned int index = 1;
		unsigned int eaten = 0;
		bool ok = false;
		string parameter_name;
		shared_ptr <Expression> parameter_value;
		parseParameter (code, index, eaten, ok, parameter_name, parameter_value);
		cout << eaten << " " << (ok? "true": "false") << " " << parameter_name << endl;
		cout << parameter_value->format () << endl << endl;
	}

	{
		string code {"[action: main;]"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseExpression (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << endl;
		cout << expression->format () << endl << endl;
	}

	{
		string code {"[action: print; main: {Hello world.};]"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseExpression (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << endl;
		cout << expression->format () << endl << endl;
	}

	{
		string code {"[Array;]"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseExpression (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << endl;
		cout << expression->format () << endl << endl;
	}

	{
		string code {"[echo; ^false;]"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseExpression (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << endl;
		cout << expression->format () << endl << endl;
	}

	{
		string code {"trans; if: $action = main;"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		auto directive = parseDirective (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << endl;
		cout << directive->format () << endl << endl;
	}

	{
		string code {"^false ^or ^false"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseExpression (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << endl;
		cout << expression->format () << endl << endl;
	}

	{
		string code {"$b ^and $b"};
		unsigned int index = 0;
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseExpression (code, index, eaten, ok);
		cout << eaten << " " << (ok? "true": "false") << endl;
		cout << expression->format () << endl << endl;
	}
}

